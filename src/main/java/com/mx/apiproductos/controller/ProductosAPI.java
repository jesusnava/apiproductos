package com.mx.apiproductos.controller;

import com.google.gson.Gson;
import com.mx.apiproductos.modelo.Producto;
import com.mx.apiproductos.repository.ProductoRepository;
import com.mx.apiproductos.services.ProductosServiceImpl;
import java.util.ArrayList;
import java.util.List;


import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

@Path("/Productos")
public class ProductosAPI {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductosAPI.class);

    ProductosServiceImpl p = new ProductosServiceImpl();
    ProductoRepository productoRepository = new ProductoRepository();

    @GET
    @Path("/BusquedaPorClave/{clave}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response busquedaProducto(@PathParam("clave") String clave) {
        //Validaciones datos de entrada
        if (clave == null) {
            return Response.status(Status.BAD_REQUEST).entity("Ingresa la clave del producto").build();
        }

        Producto producto = p.obtenerProducto(clave);
        String json = new Gson().toJson(producto);
        if (producto == null) {
            return Response.status(Status.BAD_REQUEST).entity("Producto no existente").build();
        } else {
            return Response.ok(json, MediaType.APPLICATION_JSON).build();

        }
    }

    @GET
    @Path("/BusquedaProductos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarProductos() {
        try {

            List<Producto> productos = new ArrayList();
            productos = p.listaProductos();
            String json = new Gson().toJson(productos);
            return Response.ok(json, MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NO_CONTENT).entity("Error por: " + e.toString()).build();
        }
    }

    @POST
    @Path("/RegistrarProducto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearProducto(Producto producto) throws SQLException {
        String registrado = null;
        String validacion = null;
        validacion = validadorProducto(producto);
        if (validacion != "") {
            LOGGER.error(registrado);
            return Response.status(Status.BAD_REQUEST).entity(validacion).build();
        }

        if (productoRepository.obtenerProducto(producto.getClave()) != null) {
            return Response.status(Status.BAD_REQUEST).entity("Clave ya registrada").build();
        }

        registrado = p.registroProducto(producto);
        return Response.ok(registrado).build();

    }

    @PUT
    @Path("/ActualizarProducto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarProducto(Producto producto) throws SQLException {
        String actualizado = null;
        actualizado = p.actualizacionProducto(producto);
        return Response.ok(actualizado).build();
    }

    @DELETE
    @Path("/EliminaProductos/{clave}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarProductos(@PathParam("clave") String clave) {
        //Validaciones datos de entrada
        if (clave == null) {
            LOGGER.error("No ingreso la clave producto");
            return Response.status(Status.BAD_REQUEST).entity("Ingresa la clave del producto").build();
        }

        String borrado = p.borradoProducto(clave);
        String json = new Gson().toJson(borrado);
        if (borrado == null) {
            LOGGER.error("No existe la clave producto");
            return Response.status(Status.BAD_REQUEST).entity("Producto no existente").build();
        } else {

            return Response.ok(json, MediaType.APPLICATION_JSON).build();

        }
    }

    //Validador
    String validadorProducto(Producto producto) {

        String mensaje = "";
        if (producto == null) {
            mensaje = mensaje + "Ingresa el producto.";
        }
        if (producto.getClave() == null) {
            mensaje = mensaje + "Ingresa la clave del producto.";
        }
        if (producto.getDescripcion() == null) {
            mensaje = mensaje + "Ingresa la descripcion del producto.";
        }
        if (producto.getNombre() == null) {
            mensaje = mensaje + "Ingresa el nombre del producto.";
        }
        if (producto.getPrecio() == null) {
            mensaje = mensaje + "Ingresa el precio del producto.";
        }
        if (producto.getProveedor() == null) {
            mensaje = mensaje + "Ingresa el proveedor del producto.";
        }
        return mensaje;
    }
}
