/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mx.apiproductos.config;

import java.sql.Connection;
import java.sql.DriverManager;

public class ProductosConfigDB {

    /*static {
        try {
            // CARGAR EL CONTROLADOR DE BD
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (Exception e) {
            System.out.println("[MySqlConexion] Error al cargar el driver de conexión");
            e.printStackTrace();
        }
    }
     */
    public Connection obtenerConexion() {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(
                    "jdbc:mysql://127.0.0.1:3306/dbproductos?useSSL=false&useTimeZone=true&serverTimeZone=UTC&allowPublicKeyRetrieval=true", "root", "admin");
            System.out.println("[MySqlConexion] Se va a obtener la conexión: ");
        } catch (Exception e) {
            System.out.println("[MySqlConexion] Error al obtener la conexión: ");
            e.printStackTrace();
        }
        return con;
    }
}
