package com.mx.apiproductos.repository;

import com.mx.apiproductos.config.ProductosConfigDB;
import com.mx.apiproductos.modelo.Producto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductoRepository {

    ProductosConfigDB productosConfigDB = new ProductosConfigDB();
    Connection con = productosConfigDB.obtenerConexion();

    public ArrayList<Producto> obtenerListaProductos() throws SQLException {
        ArrayList<Producto> productosList = new ArrayList<Producto>();
        PreparedStatement stmt = con.prepareStatement("SELECT * FROM productos");
        ResultSet rs = stmt.executeQuery();

        try {
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setIdproductos(rs.getInt("idproductos"));
                producto.setClave(rs.getString("clave"));
                producto.setNombre(rs.getString("nombre"));
                producto.setDescripcion(rs.getString("descripcion"));
                producto.setPrecio(rs.getFloat("precio"));
                producto.setCantidadexistente(rs.getInt("cantidadexistente"));
                producto.setProveedor(rs.getString("proveedor"));
                productosList.add(producto);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productosList;
    }

    public Producto obtenerProducto(String clave) throws SQLException {
        Producto producto = null;
        PreparedStatement stmt = con.prepareStatement("SELECT * FROM productos where clave = ?");
        stmt.setString(1, clave);
        ResultSet rs = stmt.executeQuery();

        try {
            if (rs.next()) {
                producto = new Producto();
                producto.setIdproductos(rs.getInt("idproductos"));
                producto.setClave(rs.getString("clave"));
                producto.setNombre(rs.getString("nombre"));
                producto.setDescripcion(rs.getString("descripcion"));
                producto.setPrecio(rs.getFloat("precio"));
                producto.setCantidadexistente(rs.getInt("cantidadexistente"));
                producto.setProveedor(rs.getString("proveedor"));
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return producto;
    }

    public String registrarProducto(Producto producto) throws SQLException {
        String sql = "INSERT INTO productos (clave, nombre, descripcion, precio, cantidadexistente, proveedor) "
                + "VALUES ( ?, ?, ?, ?, ? ,? )";

        Integer registrado = null;
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, producto.getClave());
            stmt.setString(2, producto.getNombre());
            stmt.setString(3, producto.getDescripcion());
            stmt.setFloat(4, producto.getPrecio());
            stmt.setInt(5, producto.getCantidadexistente());
            stmt.setString(6, producto.getProveedor());

            registrado = stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println("[MySqlServerProductoDAO] Error al intentar almacenar la información: " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                System.out.println("[MySqlServerProductoDAO] Error al intentar cerrar la conexión: " + ex.getMessage());
            }
        }

        if (registrado == 1) {
            return "Se registro correctamente";
        } else {
            return "Error al registrar";
        }

    }

    public String actualizarProducto(Producto producto) throws SQLException {

        String sql = "UPDATE productos SET nombre = ?,precio = ?,descripcion = ?,cantidadexistente = ?,proveedor = ?, clave = ? WHERE idproductos = ? ";
        Integer registrado = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
           
            stmt.setString(1, producto.getNombre());
            stmt.setFloat(2, producto.getPrecio());
            stmt.setString(3, producto.getDescripcion());
            stmt.setInt(4, producto.getCantidadexistente());
            stmt.setString(5, producto.getProveedor());
            stmt.setString(6, producto.getClave());
            stmt.setInt(7, producto.getIdproductos());
           
            registrado = stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println("[MySqlServerProductoDAO] Error al intentar actualizar la información: " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                System.out.println("[MySqlServerProductoDAO] Error al intentar cerrar la conexión: " + ex.getMessage());
            }
        }

        if (registrado == 1) {
            return "Se actualizo correctamente";
        } else {
            return "Error al actualizar";
        }

    }


    public String borrarProducto(String clave) throws SQLException {
        Integer borrado = null;
       String sql  = "DELETE FROM productos WHERE clave = ?";
        
 

        try {
             PreparedStatement stmt = con.prepareStatement(sql);
           stmt.setString(1, clave);
           
            borrado = stmt.executeUpdate();
            stmt.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("[MySqlServerProductoDAO] Error al intentar borrar la información: " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                System.out.println("[MySqlServerProductoDAO] Error al intentar cerrar la conexión: " + ex.getMessage());
            }
        }

        if (borrado == 1) {
            return "Se borro correctamente";
        } else {
            return "Error al borrar";
        }
        
    }
}
