package com.mx.apiproductos.services;

import com.mx.apiproductos.modelo.Producto;
import com.mx.apiproductos.repository.ProductoRepository;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductosServiceImpl {

    ProductoRepository productoRepository = new ProductoRepository();

    public Producto obtenerProducto(String clave) {
        Producto producto = null;
        try {
            producto = productoRepository.obtenerProducto(clave);
        } catch (SQLException ex) {
            Logger.getLogger(ProductosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return producto;
    }

    public List<Producto> listaProductos() {
        
        List<Producto> list = null;
        try {
            list = productoRepository.obtenerListaProductos();
        } catch (SQLException ex) {
            Logger.getLogger(ProductosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public String registroProducto(Producto producto) {
        String registrado = null;
        try {
            registrado = productoRepository.registrarProducto(producto);
        } catch (SQLException ex) {
            Logger.getLogger(ProductosServiceImpl.class.getName()).log(Level.SEVERE, "Error", ex);
        }

        return registrado;
    }

    public String actualizacionProducto(Producto producto) {
       String actualizado = null;
        try {
            actualizado = productoRepository.actualizarProducto(producto);
        } catch (SQLException ex) {
            Logger.getLogger(ProductosServiceImpl.class.getName()).log(Level.SEVERE, "Error", ex);
        }

        return actualizado;
    }

    public String borradoProducto(String clave) {
        String borrado = null;
        try {
            borrado = productoRepository.borrarProducto(clave);
        } catch (SQLException ex) {
            Logger.getLogger(ProductosServiceImpl.class.getName()).log(Level.SEVERE, "Error", ex);
        }

        return borrado;
    }
}
