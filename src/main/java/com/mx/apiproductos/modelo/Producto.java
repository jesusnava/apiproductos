/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mx.apiproductos.modelo;

import java.math.BigDecimal;

public class Producto {
    private Integer idproductos;
    private String clave;
    private String nombre;
    private String descripcion;
    private Float precio;
    private Integer cantidadexistente;
    private String proveedor;

    public Producto() {
    }
    
    public Producto(Integer idproductos, String clave, String nombre, String descripcion, Float precio, Integer cantidadexistente, String proveedor) {
        this.idproductos = idproductos;
        this.clave = clave;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.cantidadexistente = cantidadexistente;
        this.proveedor = proveedor;
    }

    public Integer getIdproductos() {
        return idproductos;
    }

    public void setIdproductos(Integer idproductos) {
        this.idproductos = idproductos;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    public Integer getCantidadexistente() {
        return cantidadexistente;
    }

    public void setCantidadexistente(Integer cantidadexistente) {
        this.cantidadexistente = cantidadexistente;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }
    
    


}
